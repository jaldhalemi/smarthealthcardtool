#!/usr/bin/env python3

import argparse
import base64
import json
import pathlib
import sys
from hashlib import sha256

from ellipticcurve.ecdsa import Ecdsa
from ellipticcurve.privateKey import PrivateKey, PublicKey
from ellipticcurve.signature import Signature
from ellipticcurve.utils.file import File

from remote_keystore import RemoteKeystore
from libshc import SmartHealthCard

if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--display', action='store_true',
                        help="display the contents of the input file.")
    parser.add_argument('-v', '--verify', action='store_true',
                        help="verify the signature of the input file.")
    parser.add_argument('-m', '--modify', action='store_true',
                        help="modify the input file. Requires an output file.")
    parser.add_argument('-k', '--keystore', metavar="https://link-to-alt-store/",
                        default="https://cred-provider.s3.us-west-1.amazonaws.com",
                        help="URL for the alt-store when signing the card.")
    parser.add_argument('-i', '--kid', metavar="KID",
                        default="NSBWhYOtnAuSMTXo4zfFqmkV4bjFbIC82kRKSPev_L0",
                        help="Alternate KID to use when signing the card.")
    parser.add_argument('-s', '--sign', metavar='PRIVATE_KEY.pem', type=argparse.FileType('r'),
                        help="sign the output file using the private key. Requires an output file")
    parser.add_argument('input', type=argparse.FileType('r'),
                        help="input QR image file (.png)")
    parser.add_argument('output', type=pathlib.Path, nargs='?', default=None,
                        help="output QR image file (.png)")
    args = parser.parse_args()

    # Argument sanity checks
    if args.modify and not args.output:
        parser.error("need an output file when modifying input")
    if args.sign and not args.output:
        parser.error("need an output file when signing input")
    if not args.verify and not args.modify and not args.sign:
        args.__setattr__("display", True)

    # Scan the card QR code.
    print(f"Opening card from {args.input.name}")
    card = SmartHealthCard(args.input.name)

    # Decode content
    header_json = json.loads(card.GetHeader())
    body_json = json.loads(card.GetBody())

    # Verify card..
    if args.verify:
        print("Verifying..")
        # Get the 'kid' (key ID) from the header
        kid = header_json["kid"]
        print(f"- This card uses a public key with Key ID (kid): {kid}")

        # Grab the keystore link from the "iss" link and get the public key from the specified ISS.
        keystore = body_json["iss"]
        print(
            f"- Getting data from the card's original keystore @ {keystore}")
        kh = RemoteKeystore(keystore)
        keys = kh.GetKeys()

        if kid in keys:
            print("- - Found the public key with the card's KID")
            print("\tX: " + keys[kid]["x"])
            print("\tY: " + keys[kid]["y"])
        else:
            raise Exception(f"Could not find KID {kid} in {keystore}")

        key = keys[kid]
        if card.Verify(key["x"], key["y"]):
            print("- Signature verification PASSED\n")
        else:
            print("- Signature verification FAILED\n")

    # Display content
    if args.display:
        print("Displaying card content..")
        print("Header:")
        print(json.dumps(header_json, indent=2))

        print("\nBody:")
        print(json.dumps(body_json, indent=2))

        print("\nSignature:")
        print(card.GetSignature().hex().zfill(64))
        print("")

    # Modify card content
    if args.modify:
        print("Modyfing card..")

        # Update the vaccination site to lowercase
        site = body_json["vc"]["credentialSubject"]["fhirBundle"]["entry"][1]["resource"]["performer"][
            0]["actor"]["display"]

        body_json["vc"]["credentialSubject"]["fhirBundle"]["entry"][1]["resource"]["performer"][
            0]["actor"]["display"] = site.lower()
        body_json["vc"]["credentialSubject"]["fhirBundle"]["entry"][2]["resource"]["performer"][
            0]["actor"]["display"] = site.lower()

        card.SetBody(json.dumps(
            body_json, separators=(',', ':')).encode("utf-8"))
        print("- Updated sites to lowercase\n")

    # Sign the card (and override the ISS to the private keystore)
    if args.sign:
        print("Signing..")
        print(
            f"- Overriding ISS field to the alternate keystore: {args.keystore}")
        body_json["iss"] = args.keystore

        print(f"- Overriding KID field with {args.kid}")
        header_json["kid"] = args.kid

        # Alternate keys
        alt_priv_key = PrivateKey.fromPem(File.read(str(args.sign.name)))
        alt_pub_key = alt_priv_key.publicKey()
        print("- Opened self-made alternate private key")

        def int_to_base64_str(integer):
            hex_val = hex(integer)
            bytes_val = bytes.fromhex(hex_val[2:].zfill(64))
            b64 = base64.urlsafe_b64encode(bytes_val)

            return b64.strip(bytes('=', encoding="utf-8"))

        print("- Alt public key parameters (save in jwks.json on our datastore server):")
        print("\tX: " + int_to_base64_str(alt_pub_key.point.x).decode("utf-8"))
        print("\tY: " + int_to_base64_str(alt_pub_key.point.y).decode("utf-8"))

        print("- Updating and re-signing the card with the alternate keys..")
        card.SetBody(json.dumps(
            body_json, separators=(',', ':')).encode("utf-8"))
        card.SetHeader(json.dumps(
            header_json, separators=(',', ':')).encode("utf-8"))

        card.Sign(alt_priv_key)

        print("- Verifying the new signature..")
        sig_raw = card.GetSignature()
        r = int(sig_raw[:32].hex(), 16)
        s = int(sig_raw[32:].hex(), 16)
        sig = Signature(r, s)

        if Ecdsa.verify(card.GetPayload(), sig, alt_pub_key, hashfunc=sha256):
            print("- - Signature verification PASSED\n")
        else:
            print("- - Signature verification FAILED\n")

    # Save if needed
    if args.sign or args.modify:
        print("Saving..")
        print(f"- Saving output card to {str(args.output)}..")
        card.Save(str(args.output))
        print("- Done!\n")

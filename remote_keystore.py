
import json
import urllib.request


class RemoteKeystore:
    """
    A class to get data from a remote keystore based on its "ISS" field
    """

    def __init__(self, iss):
        # Generate keys metadata filename, fetch it, and decode it.
        self.iss = iss
        self.key_url = iss + "/.well-known/jwks.json"
        self.key_json = json.loads(urllib.request.urlopen(self.key_url).read())

        # Generate a dict of 'kid' as key, and key params as values.
        self.keys = {}
        for key in self.key_json["keys"]:
            k = {}
            k["x"] = key["x"]
            k["y"] = key["y"]
            self.keys[key["kid"]] = k

    def GetKeys(self):
        return self.keys

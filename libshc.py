#!/usr/bin/env python3

import base64
import re
import zlib
from hashlib import sha256

import qrcode
from ellipticcurve.curve import prime256v1
from ellipticcurve.ecdsa import Ecdsa
from ellipticcurve.privateKey import PrivateKey, PublicKey
from ellipticcurve.signature import Signature
from PIL import Image
from pyzbar.pyzbar import decode


class SmartHealthCard:
    """
    A class to decode/encode/verify/and sign a smart health card.
    """

    def __init__(self, filename):
        self.decoded = False
        self.encoded = True
        self.filename = filename
        self.image = Image.open(filename)

    def _ImgToText(self, image):
        # Get text from QR code image file
        decoded_data = decode(image)
        if len(decoded_data) == 0:
            raise Exception("Could not decode input QR code!")

        return decoded_data[0].data.decode()

    def _TextToImg(self, text):
        # Generate QR code image object from text
        qr = qrcode.QRCode()
        qr.add_data(text)
        qr.make(fit=True)
        return qr.make_image(fill='black', back_color='white')

    def _TextToParts(self, text):
        # Split the input text into n 2-digit chunks (strip "shc:/" first)
        parts = re.findall("..", text[5:])

        # Convert each 2-digit chunk to a byte (add 45 to each part first)
        jws = ""
        for p in parts:
            jws += chr(int(p) + 45)

        # Now we have the JWS container. Items are base64-encoded groupings seperated by a period (.)
        self.jws = jws.split(".")

        # Each item is base64-encoded. Decode each item independently.
        jws_parts = list(map(self._DecodeBase64Part, self.jws))

        # Return the parts list.
        return jws_parts

    def _PartsToText(self, parts):
        # Encode each of the parts in the parts list
        self.jws = list(map(self._EncodeBase64Part, parts))

        # Create one long string containing the items seperated by a period
        jws = ".".join(self.jws)

        # Create the final text. Each byte is represented by 2-digit numbers
        text = "shc:/"
        for j in jws:
            text += "{:02d}".format(ord(j) - 45)

        return text

    def _DecodeBase64Part(self, data):
        missing_padding = len(data) % 4
        if missing_padding:
            data += "=" * (4 - missing_padding)
        return base64.urlsafe_b64decode(data)

    def _EncodeBase64Part(self, data):
        return base64.urlsafe_b64encode(data).decode(encoding="utf-8").strip('=')

    def _Decode(self):
        if self.decoded:
            return
        # Image to text
        self.text = self._ImgToText(self.image)
        # Text to base64-decoded parts
        self.parts = self._TextToParts(self.text)
        # Part0 is the header
        self.jws_header = self.parts[0]
        # Part1 is a gzip-compressed payload. Decompress it.
        self.jws_body = zlib.decompress(self.parts[1], wbits=-15)
        # Part2 is the (header + payload) signature
        self.jws_sig = self.parts[2]
        self.decoded = True

    def _Encode(self):
        if self.encoded:
            print("IMAGE ALREADY ENCODDED")
            return
        # Re-create the parts
        self.parts[0] = self.jws_header
        comp = zlib.compressobj(level=9, wbits=-15)
        self.parts[1] = comp.compress(self.jws_body)
        self.parts[1] += comp.flush()
        self.parts[2] = self.jws_sig

        # Part objects to text
        self.text = self._PartsToText(self.parts)
        # Text to image object
        self.image = self._TextToImg(self.text)._img

        self.encoded = True
        self.decoded = False

    def Verify(self, pub_key_x_b64, pub_key_y_b64):
        # Decode X, Y base64 numbers and create a public key from them.
        x = base64.urlsafe_b64decode(pub_key_x_b64+"=")
        y = base64.urlsafe_b64decode(pub_key_y_b64+"=")
        pub_key_obj = PublicKey.fromString(x + y, curve=prime256v1)

        # Get the signature binary blob and generate a Signature object from it.
        sig_raw = self.GetSignature()
        r = int(sig_raw[:32].hex(), 16)
        s = int(sig_raw[32:].hex(), 16)
        sig = Signature(r, s)

        # Verify that the payload is signed properly by using the public key.
        return Ecdsa.verify(self.GetPayload(), sig, pub_key_obj, hashfunc=sha256)

    def Sign(self, priv_key_obj):
        # Generate a new signature for the payload using the private key
        my_sig = Ecdsa.sign(self.GetPayload(), priv_key_obj)

        # Generate a binary blob from the signature
        r_raw = bytes.fromhex(hex(my_sig.r)[2:].zfill(64))
        s_raw = bytes.fromhex(hex(my_sig.s)[2:].zfill(64))
        my_sig_raw = r_raw + s_raw

        # Store the new blob in the signature part of the smart card.
        self.jws_sig = my_sig_raw

        # Re-encode everything since we've modified the card.
        self.encoded = False
        return self._Encode()

    def Save(self, filename):
        # Encode the data in case it's not up to date.
        if not self.encoded:
            self._Encode()

        # Save the image output to a file.
        self.image.save(filename)
        return True

    def GetPayload(self):
        # The signable payload is the first two parts in base64 format; seperated by a '.'
        return self.jws[0] + '.' + self.jws[1]

    def GetHeader(self):
        # The header is a json container
        self._Decode()
        return self.jws_header

    def SetHeader(self, header):
        # Set the header and update the card.
        self.jws_header = header
        self.encoded = False
        return self._Encode()

    def GetBody(self):
        # The body is a json container
        self._Decode()
        return self.jws_body

    def SetBody(self, body):
        # Update the body with a new one and update the card.
        self.jws_body = body
        self.encoded = False
        return self._Encode()

    def GetSignature(self):
        # Return the current signature.
        # The signature is a 64-byte binary blob ( sig.r | sig.s )
        self._Decode()
        return self.jws_sig

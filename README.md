# Description:
A script to allow the decoding and re-encoding of SHC-based Smart Health Cards.

# Features:
- Decode information embedded in a QR code to a .json file.
- Verify signature integrity of a card.
- Encode/update information back into a QR card.
- Sign the card using a set of self-issued alternate keys.

# Installation steps:
```text
# [optional] Create and activate a virtual enrviroment
python3 -m venv ./env
source ./env/bin/activate

# Install dependencies
pip3 install -r requirements.txt
```

# Usage:
- Generate a private key (if the `--sign` option is used):
    ```text
    openssl ecparam -name prime256v1 -genkey -out privateKey.pem
    ```
- Run the script to display, verify, modify, and/or sign an input health card:
    ```text
    ./shc_tool.py --help
    ```
- Setup a web server and expose a directory under `/.well-known/`.
- Create a `jwks.json` file based on your original card issuer's `jwks.json` file.
- Store `jwks.json` on the web server at `/.well-known/jwks.json`
- Run the script and grab the X, Y values for your public key (use the `--sign` option).
- Update the `jwks.json` file's `X` and `Y` fields with the values obtained by running the script.
- Update the keystore and sign a new card (use the `--keystore` option to set the keystore link to match your web server's URL).

# Examples:
- Display the script help
    ```text
    $ ./shc_tool.py --help

    usage: shc_tool.py [-h] [-d] [-v] [-m] [-k https:// link-to-alt-store/]
                       [-s PRIVATE_KEY.pem]
                       input [output]

    positional arguments:
      input                 input QR image file (.png)
      output                output QR image file (.png)

    optional arguments:
      -h, --help            show this help message and exit
      -d, --display         display the contents of the input   file.
      -v, --verify          verify the signature of the input   file.
      -m, --modify          modify the input file. Requires an  output file.
      -k https://link-to-alt-store/, --keystore https://    link-to-alt-store/
                            URL for the alt-store when modifying    the card.
      -s PRIVATE_KEY.pem, --sign PRIVATE_KEY.pem
                            sign the output file using the  private key. Requires
                            an output file
    ```

- Display the contents of a card
    ```text
    $ ./shc_tool.py qr_code.png
    Opening card from qr_code.png
    Displaying card content..
    Header:
    {
      "zip": "DEF",
      "alg": "ES256",
      "kid": "xxxxxxx-xxxxxxxxx"
    }

    Body:
    {
      "iss": "https://xxxxxxxxxx",
      "nbf": xxxxxx,
      "vc": {
        "type": [
          "https://smarthealth.cards#health-card",
          "https://smarthealth.cards#immunization",
          "https://smarthealth.cards#covid19"
        ],
        "credentialSubject": {
          "fhirVersion": "4.0.1",
          "fhirBundle": {
            "resourceType": "Bundle",
            "type": "collection",
            "entry": [
              {
                "fullUrl": "resource:0",
                "resource": {
                  "resourceType": "Patient",
                  "name": [
                    {
                      "family": "xxxxxxx",
                      "given": [
                        "xxxxx"
                      ]
                    }
                  ],
                  "birthDate": "xxxx-xx-xx"
                }
              },
              {
                "fullUrl": "resource:1",
                "resource": {
                  "lotNumber": "xxxxxx",
                  "resourceType": "Immunization",
                  "status": "completed",
                  "vaccineCode": {
                    "coding": [
                      {
                        "system": "http://hl7.org/fhir/sid/cvx",
                        "code": "xxx"
                      }
                    ]
                  },
                  "patient": {
                    "reference": "resource:0"
                  },
                  "occurrenceDateTime": "xxxx-xx-xx",
                  "performer": [
                    {
                      "actor": {
                        "display": "xxxxxx"
                      }
                    }
                  ]
                }
              },
              {
                "fullUrl": "resource:2",
                "resource": {
                  "lotNumber": " xxxx",
                  "resourceType": "Immunization",
                  "status": "completed",
                  "vaccineCode": {
                    "coding": [
                      {
                        "system": "http://hl7.org/fhir/sid/cvx",
                        "code": "xxx"
                      }
                    ]
                  },
                  "patient": {
                    "reference": "resource:0"
                  },
                  "occurrenceDateTime": "xxxx-xx-xx",
                  "performer": [
                    {
                      "actor": {
                        "display": "xxxxxx"
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    }

    Signature:
    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    ```
- Verify the signature of an existing card
    ```text
    $ ./shc_tool.py --verify qr_code.png
    Opening card from qr_code.png
    Verifying..
    - This card uses a public key with Key ID (kid):    xxxxx-xxxxx
    - Getting data from the card's original keystore @ xxxxxxx/creds
    - - Found the public key with the card's KID
            X: xxxxxxx
            Y: xxxxxxx
    - Signature verification PASSED
    ```

- Modify the contents of a card without re-signing
    ```text
    $ ./shc_tool.py --modify qr_code.png output.png
    Opening card from qr_code.png
    Modyfing card..
    - Updated sites to lowercase

    Saving..
    - Saving output card to output.png..
    - Done!
    ```

- Display the content of an existing card, verify its signature, modify it, then re-sign it using a private key
    ```text
    $ ./shc_tool.py --display --verify --modify --sign keys/privateKey.pem qr_code.png output.png
    OR
    $ ./shc_tool.py -d -v -m -s keys/privateKey.pem qr_code.png output.png
    Opening card from qr_code.png
    Verifying..
    - This card uses a public key with Key ID (kid):    xxxxx-xxxxx
    - Getting data from the card's original keystore @ xxxxxxx/creds
    - - Found the public key with the card's KID
            X: xxxxxxx
            Y: xxxxxxx
    - Signature verification PASSED

    Displaying card content..
    Header:
    {
      "zip": "DEF",
      "alg": "ES256",
      "kid": "xxxxxxx-xxxxxxxxx"
    }

    Body:
    {
      "iss": "https://xxxxxxxxxx",
      "nbf": xxxxxx,
      "vc": {
        "type": [
          "https://smarthealth.cards#health-card",
          "https://smarthealth.cards#immunization",
          "https://smarthealth.cards#covid19"
        ],
        "credentialSubject": {
          "fhirVersion": "4.0.1",
          ............
        }
      }
    }

    Signature:
    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    Modyfing card..
    - Updated sites to lowercase

    Signing..
    - Overriding ISS field to the alternate keystore: https://yyyyyyyyy
    - Overriding KID field with wwwwwwwwww
    - Opened self-made alternate private key
    - Alt public key parameters (save in jwks.json on our   datastore server):
            X: zzzzzzzzzz
            Y: zzzzzzzzzz
    - Updating and re-signing the card with the alternate keys..
    - Verifying the new signature..
    - - Signature verification PASSED

    Saving..
    - Saving output card to output.png..
    - Done!

    $ ./shc_tool.py -v output.png
    Opening card from output.png
    Verifying..
    - This card uses a public key with Key ID (kid):    yyyyyy-yyyyyy
    - Getting data from the card's original keystore @ https://yyyyyyyyy
    - - Found the public key with the card's KID
            X: zzzzzzzzzz
            Y: zzzzzzzzzz
    - Signature verification PASSED
    ```